## Fxcop Rules.
We shall use 3 predefined set of Microsoft rules.
1. Basic Correctness Rules rule set for managed code (https://docs.microsoft.com/en-us/visualstudio/code-quality/basic-correctness-rules-rule-set-for-managed-code?view=vs-2015)
2. Naming Warnings (https://docs.microsoft.com/en-us/visualstudio/code-quality/naming-warnings?view=vs-2015)
3. Maintainability Warnings (https://docs.microsoft.com/en-us/visualstudio/code-quality/maintainability-warnings?view=vs-2015)

**Note:** We can remove a few rules later, if required.

There are many rules, and it is not possible to go through evey one of them.
The following are some important rules from the set defined.

### CA1713: Events should not have before or after prefix
Good: `Closing`, `Closed`
Bad: `BeforeClose`, `AfterClose`


### CA1715: Identifiers should have correct prefix
Interface names should start with an uppercase 'I' followed by another uppercase letter.
Generic type parameter names should start with an uppercase 'T'

### CA1704: Identifiers should be spelled correctly
This rule parses the identifier into tokens and checks the spelling of each token.
Good: ThisIsMan
Bad: *Thi*IsMan

### CA1720: Identifiers should not contain type names
Each discrete token in the name of the parameter or member is checked against the language-specific data type names, in a case-insensitive manner
Good: ThisIsAMan
Bad: This*Int*AMan

### CA1724: Type Names Should Not Match Namespaces

### CA1707: Identifiers should not contain underscores
Good: ThisIsMan
Bad: This\_IsMan


### CA1726: Use preferred terms
This rule parses an identifier into tokens. Each token becomes a preffered term.
Eg. ArentEmpty -> AreNotEmpty

### CA1709: Identifiers should be cased correctly
Parameter names use camel casing; namespace, type, and member names use Pascal casing.

### CA1009: Declare event handlers correctly
Event handler methods take two parameters. The first is of type `System.Object` and is named `sender`. This is the object that raised the event. The second parameter is of type `System.EventArgs` and is named `e`. This is the data that is associated with the event. 
Event handler methods should not return a value.


### CA2214: Do not call overridable methods in constructors
A virtual/overridable method is a method that can be redefined in derived classes. A virtual method has an implementation in a base class as well as derived the class.


### CA2241: Provide correct arguments to formatting methods
The arguments to methods such as `WriteLine`, `Write`, and `Format` consist of a format string followed by several `System.Object` instances. Provide them properly.

### CA1308: Normalize strings to uppercase
Strings should be normalized to uppercase. A small group of characters, when they are converted to lowercase, cannot make a round trip. To make a round trip means to convert the characters from one locale to another locale.
When normalizing Strings, normalize to uppercase instead of lowercase.

### CA2104: Do not declare read only mutable reference types
The read-only modifier on a reference type field prevents the field from being replaced by a different instance of the reference type. However, the modifier does not prevent the instance data of the field from being modified through the reference type.
Read-only array fields are exempt from this rule. Due to some other rule.


### CA1501: Avoid excessive inheritance
Deeply nested type hierarchies can be difficult to follow.
A type cannot be more than four levels deep in its inheritance hierarchy.




