﻿// -----------------------------------------------------------------------
// <author> 
//      Sooraj Tom
// </author>
//
// <date> 
//      28-10-2018 
// </date>
// 
// <reviewer>
//      Axel James
// </reviewer>
//
// <copyright file="ImageProcessing.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file is the main class for Image Processing module.
//      Calls to other classes originate from here.
// </summary>
// -----------------------------------------------------------------------
namespace IITPkdB15.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Net;
    using IitPkd.SchemaTeam;
    using Networking;

    /// <summary>
    /// This is the base class for screen sharing. This class is to be instantiated by the UI team in both server and receiver.
    /// </summary>
    public class ImageProcessing : IImageProcessing
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageProcessing"/> class.
        /// The constructor for Image 
        /// </summary>
        /// <param name="communication">The communication object for networking</param>
        public ImageProcessing(ICommunication communication)
        {
            ImageSchema imageSchema = new ImageSchema();
            Compression imageCompression = new Compression();
            this.ImageCommunicator = new ImageCommunication(communication, imageSchema);
            this.ImageRecieve = new ReceiveImage(imageSchema, imageCompression, this.ImageCommunicator);
        }

        /// <summary>
        /// Gets or sets the ImageRecieve class
        /// </summary>
        public ReceiveImage ImageRecieve { get; set; }

        /// <summary>
        /// Gets or sets the Image Communicator class
        /// </summary>
        public ImageCommunication ImageCommunicator { get; set; }

        /// <summary>
        /// This method starts the screen sharing process.
        /// It is called by the server (professor) with the IP address of the client (student).
        /// </summary>
        /// <param name="clientIP">This parameter is the IP address of the target client computer</param>
        public void GetSharedScreen(string clientIP)
        {
            IPAddress clientIPAddress = IPAddress.Parse(clientIP);
            int result = this.ImageCommunicator.SignalImageModule(clientIPAddress, Signal.Start);
            //return result;
        }

        /// <summary>
        /// This method terminates the screen sharing session.
        /// </summary>
        public void StopSharedScreen()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method registers the image updater for UI.
        /// </summary>
        /// <param name="imageReceivedNotifyHandler">The updater function from UI that updates the screen shared</param>
        public void RegisterImageUpdateHandler(EventHandler<ImageEventArgs> imageReceivedNotifyHandler)
        {
            this.ImageRecieve.RegisterListenerImageDecoded(imageReceivedNotifyHandler);
        }
    }
}
