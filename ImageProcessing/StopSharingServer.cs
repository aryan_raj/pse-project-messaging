﻿//-----------------------------------------------------------------------
// <author>
//      Suman Saurav Panda
// </author>
// <reviewer>
//      Sooraj Tom
// </reviewer>
// <date>
//      1-Nov-2018
// </date>
// <summary>
//      Server side receive image class implementation 
// </summary>
// <copyright file="StopSharingServer.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace IITPkdB15.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using IitPkd.SchemaTeam;
    using Networking;

    /// <summary>
    /// This class contains the method to destroy the threads created in the server side for Image Sharing
    /// </summary>
    public class StopSharingServer
    {
        /// <summary>
        /// constructor initializes the event queue to null;
        /// </summary>
        public StopSharingServer()
        {
            this.StopSharing = null;
        }

        /// <summary>
        /// this is the event to be invoked on receiving stop signal from the UI;
        /// </summary>
        private event EventHandler StopSharing;


        /// <summary>
        /// this method would be called by main module of imaging upon getting stop signal from the UI
        /// </summary>
        public void StopReceivingImage()
        {
            this.OnStopReceivingImage();
        }

        /// <summary>
        /// this method would be called by the subscriber to listen to the stop sharing event
        /// </summary>
        /// <param name="onErrorImageDecoded"></param>
        public void RegisterListenerStopSharing(EventHandler onStopSharing)
        {
            this.StopSharing += onStopSharing;
            return;
        }

        /// <summary>
        /// the stop receiving image event invoking function
        /// </summary>
        protected virtual void OnStopReceivingImage()
        {
            this.StopSharing?.Invoke(this, EventArgs.Empty);
        }
    }
}
