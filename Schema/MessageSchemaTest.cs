﻿//-----------------------------------------------------------------------
// <copyright file="MessageSchemaTest.cs" company="B'15, IIT Palakkad">
//      Open Source. Feel free to use the code, but don't forget to acknowledge. 
// </copyright>
// <author>
//      Harsh Yadav
// </author>
// <review>
//      not reviewed
// </review>
//-----------------------------------------------------------------------

namespace IitPkd.SchemaTeam
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// Testing the functionality of imageSchema Encode function and Decode function
    /// </summary>
    public class MessageSchemaTest
    {
        /// <summary>
        /// Tests the cases described in path for image encoding and decoding
        /// </summary>
        /// <param name="path">Path to test data file </param>
        /// <returns>true if all test cases are checked</returns>
        public bool Run(string path)
        { 
            try
            {
                if (!File.Exists(path))
                {
                    throw new FileNotFoundException();
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine($"{e.ToString()}");
                return false;
            }

            string[] lines = File.ReadAllLines(path: path);
            int numberOfTests;
            try
            {
                int tryInt = int.Parse(lines[0]);
                numberOfTests = tryInt;
            }
            catch (FormatException e)
            {
                Console.WriteLine($"{e.ToString()}");
                return false;
            }

            int lineNumber = 1;
            //// reading for each test cases 
            while (numberOfTests > 0)
            {
                numberOfTests -= 1;
                int numOfKeys;
                try
                {
                    int tryInt = int.Parse(lines[lineNumber]);
                    numOfKeys = tryInt;
                }
                catch (FormatException e)
                {
                    Console.WriteLine($"{e.ToString()}");
                    return false;
                }

                lineNumber += 1;

                Dictionary<string, string> testDictionary = new Dictionary<string, string>();
                //// adding all string to string tags
                while (numOfKeys > 0)
                {
                    numOfKeys -= 1;
                    string[] keyValue = lines[lineNumber].Split(':');
                    lineNumber += 1;
                    testDictionary.Add(keyValue[0], keyValue[1]);
                }
                
                MessageSchema messageSchema = new MessageSchema();
                //// encoding data with help of imageSchema Encode function
                string encodedata = messageSchema.Encode(testDictionary);
                //// decoding data with help of imageSchema Encode function
                Dictionary<string, string> decodedDictionary = new Dictionary<string, string>(messageSchema.Decode(encodedata, false));
                //// checking all the key value pair are same or not for full decoding

                bool breakFlag = true;
                foreach (string key in testDictionary.Keys)
                {
                    if (!(decodedDictionary.ContainsKey(key) & decodedDictionary[key] == testDictionary[key]))
                    {
                        // Console.WriteLine("Failed");
                        breakFlag = false;
                        break;
                    }
                }

                if (breakFlag)
                {
                    // Console.WriteLine("Passed");
                }
            }

            return true;
        }
    }
}
